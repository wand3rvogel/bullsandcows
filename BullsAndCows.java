package bullscows;

import java.util.Random;

public class BullsAndCows {

    public static final int MAX_CODE_LENGTH = 36;
    private String secretCode;
    private final char[] chars = "0123456789abcdefghijklmnopqrstuvwxyz".toCharArray();
    
    public String prepareCode(int desiredLength, int maxSymbols) {

        generateCode(desiredLength, maxSymbols);

        return getPreparedMessage(desiredLength, maxSymbols);
    }

    private String getPreparedMessage(int desiredLength, int maxSymbols) {

        String stars = "*".repeat(desiredLength);
        StringBuilder availSymbols = new StringBuilder("(0-%c)".formatted(chars[maxSymbols - 1]));
        if (maxSymbols > 10) {
            availSymbols.insert(3,"9, a-");
        }
        return "The secret is prepared: %s %s".formatted(stars, availSymbols);
    }

    private void generateCode(int desiredLength, int maxSymbols) {

        Random rnd = new Random();

        StringBuilder secretNumber = new StringBuilder();
        while (secretNumber.length() < desiredLength) {
            char newSymbol = chars[rnd.nextInt(maxSymbols)];
            if (secretNumber.indexOf(String.valueOf(newSymbol)) == -1) {
                secretNumber.append(newSymbol);
            }
        }
        secretCode = secretNumber.toString();
    }

    public String grade(String guess) {

        int bulls = 0;
        int cows = 0;
        for (int i = 0; i < guess.length(); i++) {
            if (guess.charAt(i) == secretCode.charAt(i)) {
                bulls++;
            } else if (secretCode.indexOf(guess.charAt(i)) != -1) {
                cows++;
            }
        }

        return getBullsCowsString(bulls, cows);
    }

    private static String getBullsCowsString(int bulls, int cows) {
        StringBuilder sb = new StringBuilder();
        if (bulls > 0) {
            sb.append("%d bull%s".formatted(bulls, bulls == 1 ? "" : "s"));
        }
        if (bulls > 0 && cows > 0) {
            sb.append(" and ");
        }
        if (cows > 0) {
            sb.append("%d cow%s".formatted(cows, cows == 1 ? "" : "s"));
        }
        return sb.isEmpty() ? "None" : sb.toString();
    }
}
