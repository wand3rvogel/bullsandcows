package bullscows;

import java.util.Scanner;

public class GameService {

    private static final Scanner scanner = new Scanner(System.in);
    private final BullsAndCows bullsAndCows;
    private int codeLength;


    GameService() {
        bullsAndCows = new BullsAndCows();
    }

    public void prepare() {

        codeLength = getCodeLength();
        int numPossibleSymbols = getNumPossibleSymbols();

        String message = bullsAndCows.prepareCode(codeLength, numPossibleSymbols);
        System.out.println(message);
    }

    private int getNumPossibleSymbols() {

        System.out.println("Input the number of possible symbols in the code:");
        int num = Integer.parseInt(scanner.nextLine());
        if (num < codeLength) {
            System.out.printf("Error: it's not possible to generate a code with a length " +
                    "of %d with %d unique symbols.%n", codeLength, num);
            System.exit(0);
        }
        if (num > BullsAndCows.MAX_CODE_LENGTH) {
            System.out.println("Error: maximum number of possible symbols " +
                    "in the code is 36 (0-9, a-z).");
            System.exit(0);
        }
        return num;
    }

    private int getCodeLength() {

        System.out.println("Input the length of the secret code:");
        String input = scanner.nextLine();
        int length = 0;
        try {
            length = Integer.parseInt(input);
        } catch (NumberFormatException e) {
            System.out.printf("Error: \"%s\" isn't a valid number%n", input);
            System.exit(0);
        }
        if (length == 0 || length > BullsAndCows.MAX_CODE_LENGTH) {
            System.out.println("Error: can't generate a secret number with a length");
            System.exit(0);
        }
        return length;
    }

    public void start() {

        System.out.println("Okay, let's start a game!");
        String expected = "%d bull%s".formatted(codeLength, codeLength > 1 ? "s" : "");

        int turn = 1;
        String result;
        do {
            System.out.printf("Turn %d:%n", turn);
            result = bullsAndCows.grade(scanner.nextLine());
            System.out.printf("Grade: %s%n", result);
            turn++;
        } while (!expected.equals(result));

        System.out.println("Congratulations! You guessed the secret code.");
    }
}
