package bullscows;

public class Main {

    public static void main(String[] args) {

        GameService game = new GameService();
        game.prepare();
        game.start();
    }
}
